package com.design.map.presentation;

import com.design.map.R;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by hardik on 24/12/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class LocationPresenterTest {

    @Mock
    LocationContract.View view;

    LocationContract.Presenter presenter;

    @Before
    public void setUp() throws Exception {
        presenter = new LocationPresenter(view);
    }
    @Test
    public void fetchLocationInAbsenceOfInternet(){
        when(view.isInternetAvailable()).thenReturn(false);
        presenter.fetchLocation();
        verify(view).showErrorMessage(R.string.error_internet_needed);
    }

    @Test
    public void fetchLocation() throws Exception {
        when(view.isInternetAvailable()).thenReturn(false);
        presenter.fetchLocation();

    }

    @Test
    public void navigateToLocation() throws Exception {

    }

    @Test
    public void onLocationSelected() throws Exception {
    }

    @Test
    public void returnResult() throws Exception {
    }

}