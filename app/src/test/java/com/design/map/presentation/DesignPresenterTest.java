package com.design.map.presentation;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by hardik on 24/12/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class DesignPresenterTest {

    @Mock
    private DesignContract.View view;

    private DesignContract.Presenter<DesignContract.View> presenter;

    @Before
    public void setup(){
        presenter = new DesignPresenter(view);
    }

    @Test
    public void onFragmentClicked() throws Exception {
        int position = 1;
        when(view.getClickedFragmentPosition()).thenReturn(position);
        presenter.onFragmentClicked();
        String message = "Fragment " + position + " is selected";
        verify(view).showFragmentToast(message);
    }

    @Test
    public void onItemClicked() throws Exception {
        int position = 1;
        String description = "Item ".concat(String.valueOf(position+1));
        when(view.getClickedItemPosition()).thenReturn(position);
        presenter.onItemClicked();
        verify(view).changeDescriptionText(description);
    }

    @Test
    public void onRedButtonClicked() throws Exception {
        presenter.onRedButtonClicked();
        verify(view).changeContainerBackground(android.R.color.holo_red_light);
    }

    @Test
    public void onGreenButtonClicked() throws Exception {
        presenter.onGreenButtonClicked();
        verify(view).changeContainerBackground(android.R.color.holo_green_dark);
    }

    @Test
    public void onBlueButtonClicked() throws Exception {
        presenter.onBlueButtonClicked();
        verify(view).changeContainerBackground(android.R.color.holo_blue_light);
    }

}