package com.design.map.presentation;

import com.design.map.model.Model;
import java.util.List;

/**
 * Created by hardik on 23/12/17.
 */

interface OnLocationResultListener {

  void returnResult(List<Model> models);
}
