package com.design.map.presentation;

import android.support.annotation.ColorRes;
import android.support.annotation.NonNull;

/**
 * Created by hardik on 23/12/17.
 */

public interface DesignContract {
  interface View {
    void showFragmentToast(@NonNull String message);

    void changeContainerBackground(@ColorRes int colorResource);

    void changeDescriptionText(String message);

    int getClickedFragmentPosition();

    int getClickedItemPosition();
  }

  interface Presenter<View> {
    void onFragmentClicked();

    void onItemClicked();

    void onRedButtonClicked();

    void onGreenButtonClicked();

    void onBlueButtonClicked();
  }
}
