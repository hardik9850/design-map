package com.design.map.presentation;

import android.text.TextUtils;

import com.design.map.R;
import com.design.map.model.LocationDetail;
import com.design.map.model.Model;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by hardik on 23/12/17.
 */

public class LocationPresenter implements LocationContract.Presenter, OnLocationResultListener {

  private LocationContract.View view;
  private LocationInteractor interactor;
  private final String NOT_AVAILABLE = "Not available";

  public LocationPresenter(LocationContract.View view) {
    this.view = view;
    interactor = new LocationInteractor();
  }

  @Override public void fetchLocation() {
    boolean isInternetAvailable = view.isInternetAvailable();
    if (!isInternetAvailable) {
      view.showErrorMessage(R.string.error_internet_needed);
      return;
    }
    interactor.fetchLocation(this);
  }

  @Override public void navigateToLocation(LocationDetail location) {
    double latitude = location.getLatitude();
    double longitude = location.getLongitude();
    String locationName = location.getLocation();
    view.showLocationOnMap(latitude,longitude,locationName);
  }

  @Override public void onLocationSelected(LocationDetail location) {
    String timeByTrain = location.getTimeByTrain();
    String timeByCar = location.getTimeByCar();
    if(TextUtils.isEmpty(timeByCar)){
      timeByCar = NOT_AVAILABLE;
    }
    if(TextUtils.isEmpty(timeByTrain)){
      timeByTrain = NOT_AVAILABLE;
    }
    view.showTimeRequired(timeByCar,timeByTrain);
  }

  @Override public void returnResult(List<Model> models) {
    ArrayList<LocationDetail> locations = new ArrayList<>();
    for (Model locationModel : models) {
      Double latitude = locationModel.getLocation().getLatitude();
      Double longitude = locationModel.getLocation().getLongitude();
      String locationName = locationModel.getName();
      String timeByCar = locationModel.getFromcentral().getCar();
      String timeByTrain = locationModel.getFromcentral().getTrain();
      LocationDetail location = new LocationDetail(locationName, latitude, longitude, timeByCar, timeByTrain);
      locations.add(location);
    }
    view.onLocationDataReceived(locations);
  }
}
