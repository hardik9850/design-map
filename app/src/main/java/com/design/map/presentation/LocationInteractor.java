package com.design.map.presentation;

import com.design.map.model.Model;
import com.design.map.network.RestClient;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import java.util.List;

/**
 * Created by hardik on 23/12/17.
 */

class LocationInteractor {

    void fetchLocation(final OnLocationResultListener locationResultListener) {
        RestClient.getRestClient()
                .getLocations()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .safeSubscribe(new Observer<List<Model>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(List<Model> models) {
                        locationResultListener.returnResult(models);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
