package com.design.map.presentation;

import com.design.map.model.LocationDetail;
import java.util.List;

/**
 * Created by hardik on 23/12/17.
 */

public interface LocationContract {

  interface View{
    void onLocationDataReceived(List<LocationDetail> locations);

    boolean isInternetAvailable();

    void showErrorMessage(int errorId);

    void showLocationOnMap(double latitude,double longitude,String location);

    void showTimeRequired(String timeByCar,String timeByTrain);
  }
  interface Presenter{
    void fetchLocation();

    void navigateToLocation(LocationDetail location);

    void onLocationSelected(LocationDetail location);
  }

}
