package com.design.map.presentation;

/**
 * Created by hardik on 23/12/17.
 */

public class DesignPresenter implements DesignContract.Presenter<DesignContract.View> {

  private DesignContract.View view;

  public DesignPresenter(DesignContract.View view) {
    this.view = view;
  }

  @Override public void onFragmentClicked() {
    int position = view.getClickedFragmentPosition();
    String message = "Fragment " + position + " is selected";
    view.showFragmentToast(message);
  }

  @Override public void onItemClicked() {
    int position = view.getClickedItemPosition();
    position += 1;
    String description = "Item ".concat(String.valueOf(position));
    view.changeDescriptionText(description);
  }

  @Override public void onRedButtonClicked() {
    view.changeContainerBackground(android.R.color.holo_red_light);
  }

  @Override public void onGreenButtonClicked() {
    view.changeContainerBackground(android.R.color.holo_green_dark);
  }

  @Override public void onBlueButtonClicked() {
    view.changeContainerBackground(android.R.color.holo_blue_light);
  }
}
