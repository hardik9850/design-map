package com.design.map.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.ColorRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.OnClick;

import com.design.map.R;
import com.design.map.adapter.ItemAdapter;
import com.design.map.adapter.ViewPagerAdapter;
import com.design.map.callback.AdapterItemClickListener;
import com.design.map.presentation.DesignContract;
import com.design.map.presentation.DesignPresenter;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by hardik on 23/12/17.
 */

public class DesignFragment extends AbsFragment
        implements View.OnClickListener, AdapterItemClickListener, DesignContract.View {

    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.txt_description)
    AppCompatTextView txtDescription;
    @BindView(R.id.btn_red)
    AppCompatButton btnRed;
    @BindView(R.id.btn_blue)
    AppCompatButton btnBlue;
    @BindView(R.id.btn_green)
    AppCompatButton btnGreen;
    @BindView(R.id.container_color)
    LinearLayout colorContainer;

    private Context context;
    private String itemArray[] = new String[]{"item 1", "item 2", "item 3", "item 4", "item 5"};
    private DesignContract.Presenter presenter;
    private int fragmentPosition;
    private int itemPosition;

    public static DesignFragment getInstance() {
        return new DesignFragment();
    }

    @Override
    int getLayoutId() {
        return R.layout.fragment_design;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bindViews(view);
    }

    private void bindViews(View view) {
        presenter = new DesignPresenter(this);
        context = view.getContext();

        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getChildFragmentManager());
        viewPagerAdapter.setFragments(createFragments());
        viewPager.setAdapter(viewPagerAdapter);

        txtDescription.setText(R.string.no_item_selected);

        ItemAdapter itemAdapter = new ItemAdapter(context, this);
        itemAdapter.setData(Arrays.asList(itemArray));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(itemAdapter);
    }

    private ArrayList<AbsFragment> createFragments() {
        ArrayList<AbsFragment> arrayList = new ArrayList<>();
        arrayList.add(ViewpagerFragment.getInstance(1));
        arrayList.add(ViewpagerFragment.getInstance(2));
        arrayList.add(ViewpagerFragment.getInstance(3));
        arrayList.add(ViewpagerFragment.getInstance(4));
        return arrayList;
    }

    public void onFragmentClicked(int position) {
        fragmentPosition = position;
        presenter.onFragmentClicked();
    }

    @OnClick({R.id.btn_blue, R.id.btn_green, R.id.btn_red})
    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btn_red: {
                presenter.onRedButtonClicked();
            }
            break;
            case R.id.btn_green: {
                presenter.onGreenButtonClicked();
            }
            break;
            default:
            case R.id.btn_blue: {
                presenter.onBlueButtonClicked();
            }
            break;
        }
    }

    @Override
    public void onItemClicked(int position) {
        itemPosition = position;
        presenter.onItemClicked();
    }

    @Override
    public void showFragmentToast(@NonNull String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void changeContainerBackground(@ColorRes int colorResource) {
        colorContainer.setBackgroundColor(ContextCompat.getColor(context, colorResource));
    }

    @Override
    public void changeDescriptionText(String message) {
        txtDescription.setText(message);
    }

    @Override
    public int getClickedFragmentPosition() {
        return fragmentPosition;
    }

    @Override
    public int getClickedItemPosition() {
        return itemPosition;
    }

    @Override
    public String getFragmentName() {
        return DesignFragment.class.getName();
    }
}
