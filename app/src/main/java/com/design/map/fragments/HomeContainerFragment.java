package com.design.map.fragments;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.view.ViewPager;
import android.view.MenuItem;
import android.view.View;
import butterknife.BindView;
import com.design.map.R;
import com.design.map.adapter.ViewPagerAdapter;

import java.lang.reflect.Field;
import java.util.ArrayList;

/**
 * Created by hardik on 30/05/17.
 */

public class HomeContainerFragment extends AbsFragment {

  @BindView(R.id.bottom_navigation) BottomNavigationView bottomNavigation;
  @BindView(R.id.view_pager) ViewPager viewPager;

  private ViewPagerAdapter viewPagerAdapter;

  @NonNull public static HomeContainerFragment getInstance() {
    return new HomeContainerFragment();
  }

  @Override int getLayoutId() {
    return R.layout.fragment_home;
  }

  @Override public String getFragmentName() {
    return HomeContainerFragment.class.getName();
  }

  private ArrayList<AbsFragment> createFragments() {
    ArrayList<AbsFragment> arrayList = new ArrayList<>();
    arrayList.add(DesignFragment.getInstance());
    arrayList.add(MapFragment.getInstance());
    return arrayList;
  }

  @Override public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    viewPagerAdapter = new ViewPagerAdapter(getChildFragmentManager());
    viewPagerAdapter.setFragments(createFragments());
    viewPager.setAdapter(viewPagerAdapter);
    bottomNavigation.setBackgroundColor(getResources().getColor(R.color.colorAccent));
    bottomNavigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
      @Override
      public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

      }

      @Override public void onPageSelected(int position) {
        bottomNavigation.setSelectedItemId(
            position == 0 ? R.id.bottom_navigation_home : R.id.bottom_navigation_map);
      }

      @Override public void onPageScrollStateChanged(int state) {

      }
    });
  }


  @NonNull private BottomNavigationView.OnNavigationItemSelectedListener
      mOnNavigationItemSelectedListener =
      new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override public boolean onNavigationItemSelected(@NonNull MenuItem item) {
          switch (item.getItemId()) {
            default:
            case R.id.bottom_navigation_home:
              viewPager.setCurrentItem(0);
              return true;
            case R.id.bottom_navigation_map:
              viewPager.setCurrentItem(1);
              return true;
          }
        }
      };
}
