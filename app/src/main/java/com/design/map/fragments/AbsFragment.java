package com.design.map.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by hardik on 22/12/17.
 */

public abstract class AbsFragment extends Fragment {
  protected Unbinder unbinder;
  protected Context context;

  @Nullable @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    View view = inflater.inflate(getLayoutId(),container,false);
    unbinder = ButterKnife.bind(this, view);
    context = view.getContext();
    return view;
  }

  @Override public void onDetach() {
    super.onDetach();
    if(unbinder != null){
      unbinder.unbind();
    }
  }

  abstract public String getFragmentName();

  abstract int getLayoutId();
}
