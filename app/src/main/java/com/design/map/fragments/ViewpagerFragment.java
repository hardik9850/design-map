package com.design.map.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.design.map.R;

/**
 * Created by hardik on 22/12/17.
 */

public class ViewpagerFragment extends AbsFragment implements View.OnClickListener {

  private static final String TAG_DATA = "fragment_position";
  private int fragmentPosition;
  private final int DEFAULT_VALUE = 0;

  @BindView(R.id.txt_fragment_position) AppCompatTextView txtFragmentPosition;

  public static ViewpagerFragment getInstance(int position) {
    ViewpagerFragment viewpagerFragment = new ViewpagerFragment();
    Bundle bundle = new Bundle();
    bundle.putInt(TAG_DATA, position);
    viewpagerFragment.setArguments(bundle);
    return viewpagerFragment;
  }

  @Override int getLayoutId() {
    return R.layout.layout_simple_text;
  }

  @Override public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    Bundle bundle = getArguments();
    if (bundle != null) {
      fragmentPosition = bundle.getInt(TAG_DATA, DEFAULT_VALUE);
    }
    view.setOnClickListener(this);
    txtFragmentPosition.setText(String.valueOf(fragmentPosition));
  }

  @Override public String getFragmentName() {
    return String.valueOf(fragmentPosition);
  }

  @Override public void onClick(View view) {
    Fragment fragment = getParentFragment();
    if (fragment instanceof DesignFragment) {
      ((DesignFragment) fragment).onFragmentClicked(fragmentPosition);
    }
  }
}
