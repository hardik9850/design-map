package com.design.map.fragments;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatSpinner;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import com.design.map.R;
import com.design.map.model.LocationDetail;
import com.design.map.presentation.LocationContract;
import com.design.map.presentation.LocationPresenter;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by hardik on 23/12/17.
 */

public class MapFragment extends AbsFragment
    implements OnMapReadyCallback, LocationContract.View, View.OnClickListener,
    AdapterView.OnItemSelectedListener {

  @BindView(R.id.spinner_location) AppCompatSpinner locationSpinner;
  @BindView(R.id.txt_car_value) TextView txtByCar;
  @BindView(R.id.txt_train_value) TextView txtByTrain;
  @BindView(R.id.btn_navigate) Button btnNavigate;

  private LocationContract.Presenter locationPresenter;
  private GoogleMap googleMap;
  private SpinnerAdapter spinnerAdapter;
  private List<LocationDetail> locationDetailList = new ArrayList<>();

  @Override int getLayoutId() {
    return R.layout.fragment_map;
  }

  @Override public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);

    btnNavigate.setText(R.string.get_location);
    btnNavigate.setOnClickListener(this);
    locationPresenter = new LocationPresenter(this);
    SupportMapFragment mapFragment =
        (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
    mapFragment.getMapAsync(this);
    if(isInternetAvailable()){
      locationPresenter.fetchLocation();
    }
  }

  @Override public void onMapReady(GoogleMap googleMap) {
    this.googleMap = googleMap;
  }

  @Override public void onLocationDataReceived(List<LocationDetail> locationDetails) {
    locationDetailList = locationDetails;
    ArrayList<String> arrayList = new ArrayList<>();
    for (LocationDetail locationDetail : locationDetails) {
      arrayList.add(locationDetail.getLocation());
    }
    spinnerAdapter =
        new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item, arrayList);
    locationSpinner.setAdapter(spinnerAdapter);
    locationSpinner.setOnItemSelectedListener(this);
    btnNavigate.setText(R.string.navigate);
  }

  @Override public void showErrorMessage(int errorId) {
    Toast.makeText(context, errorId, Toast.LENGTH_SHORT).show();
    btnNavigate.setText(R.string.get_location);
  }

  @Override public void showLocationOnMap(double latitude, double longitude, String location) {
    if (googleMap == null) return;
    googleMap.addMarker(
        new MarkerOptions().position(new LatLng(latitude, longitude)).title(location));
    CameraUpdate cameraUpdate =
        CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 13f);
    googleMap.moveCamera(cameraUpdate);
  }

  @Override public void showTimeRequired(String timeByCar, String timeByTrain) {
    txtByCar.setText(timeByCar);
    txtByTrain.setText(timeByTrain);
  }

  @Override public void onClick(View view) {
    if (btnNavigate.getText().toString().equals(getString(R.string.get_location))) {
      locationPresenter.fetchLocation();
    } else {
      int position = locationSpinner.getSelectedItemPosition();
      locationPresenter.navigateToLocation(locationDetailList.get(position));
    }
  }

  @Override public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
    LocationDetail locationDetail = locationDetailList.get(i);
    locationPresenter.onLocationSelected(locationDetail);
  }

  @Override public void onNothingSelected(AdapterView<?> adapterView) {

  }

  @Override public boolean isInternetAvailable() {
    ConnectivityManager connectivityManager =
        (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

    NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
    if (networkInfo == null) {
      return false;
    }
    return networkInfo.isConnected();
  }

  @Override public String getFragmentName() {
    return MapFragment.class.getName();
  }

  public static AbsFragment getInstance() {
    return new MapFragment();
  }
}
