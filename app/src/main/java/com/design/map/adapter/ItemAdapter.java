package com.design.map.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.design.map.R;
import com.design.map.callback.AdapterItemClickListener;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by hardik on 22/12/17.
 */

public class ItemAdapter extends RecyclerView.Adapter<ItemViewHolder> implements
    AdapterItemClickListener {
  private ArrayList<String> arrayList = new ArrayList<>();
  private Context context;
  private AdapterItemClickListener adapterItemClickListener;

  public ItemAdapter(Context context,AdapterItemClickListener adapterItemClickListener){
    this.context = context;
    this.adapterItemClickListener = adapterItemClickListener;
  }
  public void setData(List<String> arrayList){
    this.arrayList.addAll(arrayList);
    notifyDataSetChanged();
  }
  @Override public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(context).inflate(R.layout.adapter_item,null);
    return new ItemViewHolder(view,this);
  }

  @Override public void onBindViewHolder(ItemViewHolder holder, int position) {
    holder.txtItem.setText(arrayList.get(position));
  }

  @Override public int getItemCount() {
    return arrayList.size();
  }

  @Override public void onItemClicked(int position) {
      if(adapterItemClickListener != null){
        adapterItemClickListener.onItemClicked(position);
      }
  }
}
