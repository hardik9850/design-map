package com.design.map.adapter;

import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import com.design.map.R;
import com.design.map.callback.AdapterItemClickListener;

/**
 * Created by hardik on 22/12/17.
 */

class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
  public AppCompatTextView txtItem;
  private AdapterItemClickListener adapterItemClickListener;
  public ItemViewHolder(View itemView,AdapterItemClickListener adapterItemClickListener) {
    super(itemView);
    txtItem = itemView.findViewById(R.id.txt_item);
    itemView.setOnClickListener(this);
    this.adapterItemClickListener = adapterItemClickListener;
  }

  @Override public void onClick(View view) {
    int position = getAdapterPosition();
    if(position == -1)
      return;
     if(adapterItemClickListener != null){
       adapterItemClickListener.onItemClicked(position);
     }
  }
}
