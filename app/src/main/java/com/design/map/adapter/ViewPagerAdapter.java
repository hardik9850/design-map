package com.design.map.adapter;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import com.design.map.fragments.AbsFragment;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by hardik on 22/12/17.
 */

public class ViewPagerAdapter extends FragmentStatePagerAdapter {

  private List<AbsFragment> fragmentsList = new ArrayList<>();

  public ViewPagerAdapter(FragmentManager fm) {
    super(fm);
  }

  public void setFragments(ArrayList<AbsFragment> arrayList) {
    fragmentsList.addAll(arrayList);
  }

  @Override public Fragment getItem(int position) {
    return fragmentsList.get(position);
  }

  @Override public int getCount() {
    return fragmentsList.size();
  }

  @Nullable @Override public CharSequence getPageTitle(int position) {
    return fragmentsList.get(position).getFragmentName();
  }
}
