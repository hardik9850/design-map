package com.design.map;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import com.design.map.fragments.HomeContainerFragment;

/**
 * Created by hardik on 23/12/17.
 */

public class HomeActivity extends AppCompatActivity {
  @Override protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_home);
    HomeContainerFragment homeContainerFragment = HomeContainerFragment.getInstance();
    getSupportFragmentManager().beginTransaction()
        .add(R.id.fragment_container, homeContainerFragment,homeContainerFragment.getFragmentName())
        .commit();
  }
}
