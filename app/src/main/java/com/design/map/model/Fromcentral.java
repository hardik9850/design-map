
package com.design.map.model;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Fromcentral {

    @SerializedName("car")
    private String mCar;
    @SerializedName("train")
    private String mTrain;

    public String getCar() {
        return mCar;
    }

    public void setCar(String car) {
        mCar = car;
    }

    public String getTrain() {
        return mTrain;
    }

    public void setTrain(String train) {
        mTrain = train;
    }

}
