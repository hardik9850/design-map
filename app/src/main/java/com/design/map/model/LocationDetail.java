package com.design.map.model;

/**
 * Created by hardik on 23/12/17.
 */

public class LocationDetail {
  double latitude;
  String location;
  double longitude;
  String timeByCar;
  String timeByTrain;

  public LocationDetail(String locationName, Double latitude, Double longitude, String timeByCar,
      String timeByTrain) {
    this.location = locationName;
    this.latitude = latitude;
    this.longitude = longitude;
    this.timeByTrain = timeByTrain;
    this.timeByCar = timeByCar;
  }

  public String getTimeByCar() {
    return timeByCar;
  }

  public void setTimeByCar(String timeByCar) {
    this.timeByCar = timeByCar;
  }

  public String getTimeByTrain() {
    return timeByTrain;
  }

  public void setTimeByTrain(String timeByTrain) {
    this.timeByTrain = timeByTrain;
  }

  public String getLocation() {
    return location;
  }

  public void setLocation(String location) {
    this.location = location;
  }


  public double getLatitude() {
    return latitude;
  }

  public void setLatitude(double latitude) {
    this.latitude = latitude;
  }

  public double getLongitude() {
    return longitude;
  }

  public void setLongitude(double longitude) {
    this.longitude = longitude;
  }
}
