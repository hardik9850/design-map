
package com.design.map.model;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Model {

    @SerializedName("fromcentral")
    private Fromcentral mFromcentral;
    @SerializedName("id")
    private Long mId;
    @SerializedName("location")
    private Location mLocation;
    @SerializedName("name")
    private String mName;

    public Fromcentral getFromcentral() {
        return mFromcentral;
    }

    public void setFromcentral(Fromcentral fromcentral) {
        mFromcentral = fromcentral;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public Location getLocation() {
        return mLocation;
    }

    public void setLocation(Location location) {
        mLocation = location;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

}
