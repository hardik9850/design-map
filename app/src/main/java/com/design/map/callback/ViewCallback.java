package com.design.map.callback;

/**
 * Created by hardik on 22/12/17.
 */

public interface ViewCallback {
  void onViewClicked(int position);
}
