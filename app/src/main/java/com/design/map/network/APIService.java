package com.design.map.network;

import com.design.map.model.Model;
import io.reactivex.Observable;
import java.util.List;
import retrofit2.http.GET;

/**
 * Created by hardik on 23/12/17.
 */

public interface APIService {

  @GET("sample.json") Observable<List<Model>> getLocations();
}
